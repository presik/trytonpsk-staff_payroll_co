# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

PAYMENTS = [
    'salary', 'bonus', 'reco', 'recf', 'hedo', 'heno', 'dom', 'hedf', 'henf',
]

SOCIAL_SEGURITY = [
    'risk', 'health', 'retirement', 'box_family', 'sena', 'icbf',
]

LIM_UVT_DEDUCTIBLE = {
    'fvp_ind': Decimal(2500 / 12),
    'afc_fvp': Decimal(3800 / 12),
    'housing_interest': 100,
    'health_prepaid': 16,
    'dependents': 32,
    'exempted_income': Decimal(790 / 12),
    'renta_deductions': Decimal(1340 / 12),
}

LIM_PERCENT_DEDUCTIBLE = {
    'fvp_ind': 25,
    'dependents': 10,
    'afc_fvp': 30,
    'exempted_income': 25,
    'renta_deductions': 40,
}

FIELDS_AMOUNT = [
    'salary',
    'reco',
    'recf',
    'hedo',
    'heno',
    'dom',
    'hedf',
    'henf',
    'cost_reco',
    'cost_recf',
    'cost_hedo',
    'cost_heno',
    'cost_dom',
    'cost_hedf',
    'cost_henf',
    'bonus',
    'total_extras',
    'gross_payment',
    'health',
    'retirement',
    'food',
    'transport',
    'fsp',
    'retefuente',
    'other_deduction',
    'total_deduction',
    'ibc',
    'net_payment',
    'box_family',
    'box_family',
    'unemployment',
    'interest',
    'holidays',
    'bonus_service',
    'discount',
    'other',
    'total_benefit',
    'risk',
    'health_provision',
    'retirement_provision',
    'total_ssi',
    'total_cost',
    'sena',
    'icbf',
    'acquired_product',
]

EXTRAS = [
    'reco',
    'recf',
    'hedo',
    'heno',
    'dom',
    'hedf',
    'henf',
]

SHEET_SUMABLES = [
    'salary',
    'total_extras',
    'transport',
    'food',
    'bonus',
    'unemployment',
    'interest',
    'holidays',
    'bonus_service',
    'fsp',
    'retefuente',
    'total_deduction',
    'retirement_provision',
    'other_deduction',
    'acquired_product',
    'health_provision',
    'cost_reco',
    'cost_recf',
    'cost_hedo',
    'cost_heno',
    'cost_dom',
    'cost_hedf',
    'cost_henf',
]

SHEET_SUMABLES.extend(SOCIAL_SEGURITY)
SHEET_SUMABLES.extend(EXTRAS)

SHEET_FIELDS_NOT_AMOUNT = [
    'item',
    'employee',
    'id_number',
    'position',
    'legal_salary',
    'salary_day',
    'salary_hour',
    'worked_days',
    'period',
    'department',
]

CONCEPTS_ELECTRONIC = [
    'VacacionesComunes', 'IncapacidadComun',
    'IncapacidadProfesional', 'IncapacidadLaboral',
    'LicenciaMP', 'LicenciaR', 'LicenciaNR']
