import macros
import nimpy

macro eval(value: static[string]): untyped =
  result = parseStmt value

# let e: int = 5
# echo eval("1 + e")

# type Contract = object
#   employee: int
#   start_date: string
#   end_date: string
#   salary: float

# type
#   WageType = tuple[id: int, name: string]



type
  WageType = object
    id: int
    name: string
    sequence: int
    definition: string
    # unit_price_formula: void
    # expense_formula: void
    # salary_constitute: bool
    # uom: int
    # type_concept: string
    # concepts_salary: @[]
    # adjust_days_worked: bool

# type Payroll = object
#   employee: int
#   start_date: string
#   end_date: string
  # salary: float
  # lines: @[PayrollLine]


# proc computePayroll(wages: seq[WageType]): int {.exportpy.} =

proc computePayroll(wages: seq[WageType]): string {.exportpy.} =
    # echo wage.name
    for waget in wages:
      echo waget.name
      echo waget.sequence
    # var mat = Payroll(
    #   employee: 456,
    #   start_date: "2023-01-01",
    #   end_date: "2023-01-31"
    # )
    # let mat: string = wage.name
    # return mat
