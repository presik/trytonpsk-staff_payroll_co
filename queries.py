detailed_payroll_report = """
    SELECT sp.contract, pp.name AS name, pp.id_number, pu.name as unit, SUM(spl.quantity) quantity, SUM(spl.unit_value)/count(*) prom_unit_value, swt.name, swt.definition
    FROM staff_payroll AS sp
    INNER JOIN company_employee AS ce
    ON sp.employee = ce.id
    INNER JOIN party_party AS pp
    ON pp.id = ce.party
    INNER JOIN staff_payroll_line AS spl
    ON sp.id = spl.payroll
    INNER JOIN product_uom AS pu
    ON spl.uom = pu.id
    INNER JOIN staff_wage_type AS swt
    ON swt.id = spl.wage_type
    WHERE sp.start>='{start_period}' and sp.end<='{end_period}' {additional_conditions}
    GROUP BY
        sp.contract, pp.name, pp.id_number, pu.name, swt.name, swt.definition
    ORDER BY
        pp.name;
"""


detailed_payroll_by_party = """SELECT pp_l.name, pp_l.id_number, pp.name, pp.id_number, pu.name as unit, SUM(spl.quantity) quantity, SUM(spl.unit_value)/count(*) prom_unit_value, swt.name, swt.definition
    FROM staff_payroll AS sp
    INNER JOIN company_employee AS ce
    ON sp.employee = ce.id
    INNER JOIN party_party AS pp
    ON pp.id = ce.party
    INNER JOIN staff_payroll_line AS spl
    ON sp.id = spl.payroll
    INNER JOIN party_party AS pp_l
    ON pp_l.id = spl.party
    INNER JOIN product_uom AS pu
    ON spl.uom = pu.id
    INNER JOIN staff_wage_type AS swt
    ON swt.id = spl.wage_type
    WHERE sp.start>='{start_period}' and sp.end<='{end_period}' {additional_conditions}
    GROUP BY
        pp_l.name, pp_l.id_number, pp.name, pp.id_number, pu.name, swt.name, swt.definition
    ORDER BY
        pp_l.name, pp.name;"""


detailed_payroll_by_line = """SELECT pp.name, pp.id_number, aaa.code, aaa.name, swt.name, pu.name as unit, SUM(spl.quantity) quantity, SUM(spl.unit_value)/count(*) prom_unit_value, swt.definition
, pp_l.name, pp_l.id_number
FROM staff_payroll AS sp
    INNER JOIN company_employee AS ce
    ON sp.employee = ce.id
    INNER JOIN party_party AS pp
    ON pp.id = ce.party
    INNER JOIN staff_payroll_line AS spl
    ON sp.id = spl.payroll
    INNER JOIN party_party AS pp_l
    ON pp_l.id = spl.party
    INNER JOIN product_uom AS pu
    ON spl.uom = pu.id
    INNER JOIN staff_wage_type AS swt
    ON swt.id = spl.wage_type
    INNER JOIN staff_payroll_mandatory_wage AS spmw
    ON spmw.wage_type = swt.id
    INNER JOIN analytic_account_account AS aaa
    ON spmw.analytic_account = aaa.id
    WHERE sp.start>='{start_period}' and sp.end<='{end_period}' {additional_conditions}
GROUP BY
        pp.name, pp.id_number, aaa.code, aaa.name, swt.name, pu.name, pp_l.name, pp_l.id_number, swt.definition

ORDER BY
        pp_l.name, pp.name, aaa.code;"""
