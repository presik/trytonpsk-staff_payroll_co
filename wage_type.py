# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pyson import Eval

ROUND_AMOUNTS = [
    ('', ''),
    ('above_amount', 'Above Amount'),
    ('under_amount', 'Under Amount'),
    ('automatic', 'Automatic'),
]


class WageType(metaclass=PoolMeta):
    __name__ = 'staff.wage_type'
    minimal_amount = fields.Numeric('Minimal Amount')
    month_application = fields.Boolean('Month Application')
    apply_special_salary = fields.Boolean('Apply Special Salary')
    adjust_days_worked = fields.Boolean('Adjust Days Worked',
        help='If is true, rounded month work days on payroll to 30 indeed '
            'if month has 31 days, or rounded to 15 indeed the biweekly'
            'pay has 16 days')
    round_amounts = fields.Selection(ROUND_AMOUNTS, 'Rounds amounts',
        help='Approximates the highest value in hundreds')
    provision_cancellation = fields.Many2One('staff.wage_type', 'Cancellation of Provision')
    # wage_type_60_40 = fields.Many2One('staff.wage_type', 'Wage Type 60/40', help='select wage type for set 60/40')
    account_60_40 = fields.Many2One('account.account', 'Account 60/40',
        domain=[
                ('type', '!=', None),
                ('company', '=', Eval('context', {}).get('company', 0))],
        states={
            'invisible': ~Eval('context', {}).get('company'),
        }, help='select wage type for set 60/40')

    @classmethod
    def __setup__(cls):
        super(WageType, cls).__setup__()
        new_sel = [
            ('health', 'Health'),
            ('retirement', 'Retirement'),
            ('risk', 'Risk'),
            ('box_family', 'Box Family'),
            ('commission', 'Commission'),
            ('bonus_service', 'Bonus Service'),
            ('food', 'Food'),
            ('unemployment', 'Unemployment'),
            ('allowance', 'Allowance'),
            ('transport', 'Transport'),
            ('interest', 'Interest'),
            ('bonus', 'Bonus'),
            ('tax', 'Tax'),
            ('holidays', 'Holidays'),
            ('syndicate', 'Syndicate'),
            ('fsp', 'FSP'),
            ('sena', 'SENA'),
            ('icbf', 'ICBF'),
            ('acquired_product', 'Acquired Product'),
            ('loan', 'Loan'),
            ('advance', 'Advance'),
            ('convencional_bonus', 'Convencional Bonus'),
            ('vacation_bonus', 'Vacation Bonus'),
            ('room_bonus', 'Room Bonus'),
            ('salary_readjustment', 'Salary Readjustment'),
            ('non_salary_readjustment', 'Non-Salary Readjustment'),
            ('non_salary_bonus', 'Non-Salary Bonus'),
            ('incapacity_greater_to_2_days', 'Incapacity greater to 2 days'),
            ('incapacity_less_to_2_days', 'Incapacity less to 2 days'),
            ('incapacity_arl', 'Incapacity Arl'),
        ]
        if new_sel not in cls.type_concept.selection:
            cls.type_concept.selection.extend(new_sel)

    @classmethod
    def __register__(cls, module_name):
        super(WageType, cls).__register__(module_name)
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('amount_required'):
            table_h.drop_column('amount_required')
        # cursor = Transaction().connection.cursor()
        # cursor.execute(
        #     'ALTER TABLE staff_wage_type DROP COLUMN IF EXISTS amount_required'
        # )


class WageTypeReport(Report):
    __name__ = 'staff.wage_type.report'
