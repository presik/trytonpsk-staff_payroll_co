# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import date

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.modules.staff_payroll.exceptions import MissingPartyWageType


_ZERO = Decimal('0.0')

class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['staff.liquidation']

class SeverancePayClearingStart(ModelView):
    'Severance Pay Clearing Start'
    # Consolidacion de Cesantias
    __name__ = 'staff_payroll.severance_pay_clearing.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    journal = fields.Many2One('account.journal', 'Journal', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    severance_pay = fields.Many2One('account.account',
        'Severance Pay', domain=[
                ('type', '!=', None),
        ], required=True)
    counterpart_account = fields.Many2One('account.account',
        'Counterpart Account', domain=[
                ('type', '!=', None),
        ], required=True)
    description = fields.Char('Description', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class SeverancePayClearing(Wizard):
    'Severance Pay Clearing'
    __name__ = 'staff_payroll.severance_pay_clearing'
    start = StateView('staff_payroll.severance_pay_clearing.start',
        'staff_payroll_co.severance_pay_clearing_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'create_', 'tryton-print', default=True),
        ])
    create_ = StateTransition()
    done = StateView('staff_payroll.severance_pay_clearing.done',
        'staff_payroll_co.severance_pay_clearing_done_view_form', [
            Button('Done', 'end', 'tryton-ok', default=True),
        ])

    @classmethod
    def __setup__(cls):
        super(SeverancePayClearing, cls).__setup__()

    def default_done(self, fields):
        return {'result': self.result}

    def transition_create_(self):
        pool = Pool()
        MandatoryWage = pool.get('staff.payroll.mandatory_wage')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Period = pool.get('account.period')
        move_lines = MoveLine.search([
            ('move.period.fiscalyear', '=', self.start.fiscalyear),
            ('account', '=', self.start.severance_pay.id),
            ('party', '!=', None),
            ('reconciliation', '=', None),
            ('credit', '>', 0)
        ])

        mandatory_wages = MandatoryWage.search([
            ('employee', '!=', None),
            ('party', '!=', None),
            ('wage_type.type_concept', '=', 'retirement'),
        ])
        parties_related = {}
        for mw in mandatory_wages:
            if not mw or not mw.party or not mw.employee.party:
                raise MissingPartyWageType(
                    gettext('staff_payroll_co.msg_missing_party',
                            wage=mw.wage_type.name,
                            employee=mw.employee.party.name))
            parties_related[mw.employee.party.id] = mw.party.id

        counterparts = {}
        lines_to_create = []
        to_reconcile = {}

        date_ = date.today()

        move, = Move.create([{
            'period': Period.find(self.start.company.id, date=date_),
            'date': date_,
            'journal': self.start.journal.id,
            'state': 'draft',
            'description': self.start.description,
        }])

        grouped_parties_lines = {}
        for line in move_lines:
            party_id = line.party.id
            if grouped_parties_lines.get(party_id):
                grouped_parties_lines[party_id]['debit'] += line.credit
                to_reconcile[party_id].append(line)
            else:
                to_reconcile[party_id] = [line]
                grouped_parties_lines[party_id] = {
                    'account': line.account.id,
                    'party': party_id,
                    'debit': line.credit,
                    'credit': 0,
                    'description': line.description,
                    'move': move.id,
                }

        for party_id, line in grouped_parties_lines.items():
            line, = MoveLine.create([line])
            to_reconcile[party_id].append(line)
            _party_id = parties_related.get(party_id)
            if not _party_id:
                continue
            MoveLine.create([{
                'account': self.start.counterpart_account.id,
                'party': _party_id,
                'debit': 0,
                'credit': line.debit,
                'move': move.id,
            }])

        for lr in to_reconcile.values():
            MoveLine.reconcile(lr)

        self.result = UserError(
            gettext('staff_payroll_co.msg_move_created',
                    s=move.number))
        return 'done'


class SeverancePayClearingDone(ModelView):
    'Severance Pay Clearing Done'
    __name__ = 'staff_payroll.severance_pay_clearing.done'
    result = fields.Text('Result', readonly=True)
