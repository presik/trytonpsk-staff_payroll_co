# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Event(metaclass=PoolMeta):
    "Staff Event"
    __name__ = 'staff.event'

    lines_payroll = fields.One2Many('staff.payroll.line', 'origin',
        'Lines Payroll', readonly=True)
